const express = require('express');
const nj = require('nunjucks');
const data = require('./data');
const app = express();

nj.configure('views', {
  autoescape: true,
  express: app
});

app.use(express.json()) // for parsing application/json
app.use(express.urlencoded({ extended: true })) // for parsing application/x-www-form-urlencoded

app.use(express.static('views'));

app.get('/', function(req, res) {
  res.render('index.html');
});

const qrys = data.qrys();

const inspectInput = function (req, res, next) {
  console.log(req.query);
  console.log(req.body);
  next();
};
const solicitParams = function (req, res, next) {
  const qry = (req.body && req.body.qry) ? req.body.qry : '';
  let paramCount = 0;
  //detect if the user filled in the holes
  //build the form based on qry while we are looping
  const tokens = qry.split(' ').map((token, n) => {
    if ('?' == token) {
      if (req.body[`p-${n}`]) paramCount += 1;
      return `<input type=text name="p-${n}" >`;
    }
    return token;
  });
  //what happens if qry takes no param? use LIMIT 1?
  if (qry && 0 === paramCount) {
    tokens.push(`<input type=hidden name=qry value="${qry}" >`);// do not forget to pass along qry
    res.render('qry-fill-in-params.html', {
      tokens: tokens
    });
  } else {
    next();
  }
};
const handleParams = function(req, res, next) {
  const qry = (req.body && req.body.qry) ? req.body.qry : '';
  const vals = [];
  qry.split(' ').forEach((token, n) => {
    if ('?' == token) {
      vals.push(req.body[`p-${n}`]);
    }
  });
  //time for SQL
  data.select(qry, vals).then(rows => {
    res.render('qry-list.html', {
      qry: qry,
      qrys: qrys,
      results: rows 
    });
  }).catch(err => {
    res.end(err.message);
  });
};
app.all('/q', [inspectInput, solicitParams, handleParams]);

const server = app.listen(3000, () => {
  console.log('server started');
});
