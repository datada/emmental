const sqlite3 = require('sqlite3').verbose();
const fs = require('fs');

// add to the following list if there is more 
const qrySqls = [ 
   ["Colleges from state = ?",  "SELECT * FROM College WHERE state = ?"],
   ["Studs with GPA higher than ?",  "SELECT * FROM Student WHERE GPA > ?"],
   ["Studs from HS enrollment larger than > ?",  "SELECT * FROM Student WHERE sizeHS > ?"],
   ["Studs GPA higher than ? and from HS enrollment larger than ?", "SELECT * FROM Student WHERE GPA > ? AND sizeHS > ?"],
   ["Applications by decision ? (Y or N)",  `SELECT Student.sID, Student.sName, Apply.cName, Apply.major, Apply.decision FROM Student, Apply WHERE Student.sID = Apply.sID AND Apply.decision == ?`],
   ["Movies with title like ? (e.g. %gone%)", "SELECT * FROM movies WHERE title LIKE ?"],
   ["Movies with genres like ? (e.g. %drama%)", "SELECT * FROM movies WHERE genres LIKE ?"],
   [ "Movies rated above >= ? (1 thru 5) by gender ? (F or M) LIMIT ?", `SELECT title, rating, gender FROM movies INNER JOIN ratings USING (mid) INNER JOIN users USING (uid) WHERE rating >= ? AND gender = ? LIMIT ?`]
];
// String [Any] -> Promise.then(rows => {})
const select = function (qry, params) {
  if (!qry) return Promise.resolve([]);
  
  let sql;
  qrySqls.forEach(row => {
      if (qry == row[0]) sql = row[1];
  });
  if (!sql) return Promise.resolve([]);

  return new Promise((resolve, reject) => {
    let db = new sqlite3.Database('./demo.db', (err) => {
        if (err) return console.error(err.message);
        console.log('Connected to a SQlite database.');
    });
    fs.readFile('./demo.sql', 'utf8', (e, data) => {
      db.exec(data, er => {
        db.all(sql, params, (err, rows) => {
            if (err) reject(err);
            if (rows)  resolve(rows);
            resolve([]);
        });
      });
    });
  }).then(rows => {
    //console.log(rows);
    const tabular = [];
    if (0 < rows.length) {
      const keys = Object.keys(rows[0]);
      tabular.push(keys);
      rows.forEach(row => {
        tabular.push(keys.map(key => {
          return row[key];
        }));
      }); 
    }
    return tabular;
  }).then(table => {
    return table;
  });
};

const qrys = function () {
    return qrySqls.map(row => {
        return row[0];
    });
};

exports.select = select;
exports.qrys = qrys;
exports.test = function () {
  select('', [])
    .then(console.log).catch(console.error);
  select("Studs GPA higher than ? and from HS enrollment larger than ?", [3.5, 900])
    .then(console.log).catch(console.error);
  select("Movies rated above >= ? (1 thru 5) by gender ? (F or M) LIMIT ?", [5, "F", 10])
    .then(console.log).catch(console.error);
};
