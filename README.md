# Query with holes

The first view presents the user some prebuilt queries such as "GPA higher than ? and HS enrollment larger than ?"

When the user selects it, another view is presented with ?s replaced with html input.

When the user fills in the inputs, SQL is completed and executed. Finally, any fetched rows are presented.

## Required

racket (tested on 5.3.1+), sqlite3

Create a directory named "rkt-web-urls" in the same directory as main.rkt

Run main.rkt from DrRacket or

```sh
# usage
racket main.rkt
```

Type something into the input box to trigger autocomplete. Can convert HTML to CSV or JSON. Columns can be sorted. Sorry but the really interesting joins take too long!


## Note to self

Re-opened movies.dat in Sublime as UTF-8 and saved before loading that table into sqlite3 to deal with the issue between racket and sqlite3.

Added occupations table.

Added some applications to State U Film school.

Really interesting joins take forever.
